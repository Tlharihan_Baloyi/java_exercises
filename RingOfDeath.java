import java.util.ArrayList;

/**
 * Created by Mbhurhi on 10/7/2015.
 */
public class RingOfDeath {
    public static int execute(int n, int m){
        int killIdx = 0;
        ArrayList<Integer> prisoners = new ArrayList<Integer>(n);
        for(int i = 0;i < n;i++){
            prisoners.add(i);
        }
        System.out.println("Prisoners executed in order:");
        while(prisoners.size() > 1){
            killIdx = (killIdx + m - 1) % prisoners.size();
            System.out.print(prisoners.get(killIdx) + " ");
            prisoners.remove(killIdx);
        }
        System.out.println();
        return prisoners.get(0);
    }



    public static void main(String[] args){
        System.out.println("Survivor: " + execute(41, 3));
    }
}
