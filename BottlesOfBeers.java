/**
 * Created by Mbhurhi on 9/18/2015.
 */
public class BottlesOfBeers {


    public static void main(String[] args)
    {
        int noOfBeers = 99;

        while(noOfBeers >= 0)
        {
            System.out.println(noOfBeers + "\t bottles of beer on the wall");
            System.out.println(noOfBeers + "\t bottles of beer");

            if(noOfBeers != 0)
            {
                System.out.println("Take one down, and pass it around");
            }
            noOfBeers--;
        }
    }
}
