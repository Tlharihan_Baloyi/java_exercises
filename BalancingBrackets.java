import java.util.Random;

/**
 * Created by Mbhurhi on 10/6/2015.
 */
public class BalancingBrackets {
    public static void main(String[] args)
    {
        guessNoOfBrackets();
    }
    public static void guessNoOfBrackets()
    {
        String[] brackets = {"(empty)","[]","][","[][]","][][","[[][]]","[]][[]"};
        String[] check = {"ok","ok","not ok","ok","not ok","ok","not ok"};


        Random ran = new Random();
        int value = ran.nextInt(7);

        System.out.println(brackets[value] + " " + check[value]);
        //return value;
    }
}
