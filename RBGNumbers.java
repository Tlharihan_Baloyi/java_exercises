import com.lowagie.text.Document;

import java.awt.*;
import java.util.Scanner;


/**
 * Created by Mbhurhi on 9/29/2015.
 */
public class RBGNumbers {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter red value>>>");
        int red = scan.nextInt();
        System.out.print("Please enter blue value>>>");
        int blue = scan.nextInt();
        System.out.print("Please enter green value>>>");
        int green = scan.nextInt();
        String display = toHexa(red)+toHexa(blue)+toHexa(green).toUpperCase();
        System.out.println("#" + display);

    }
    public static String toHexa(int  value)
    {
        String hexa  = Integer.toHexString(value);

        return hexa;
    }
}
