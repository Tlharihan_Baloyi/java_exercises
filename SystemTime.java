import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mbhurhi on 9/23/2015.
 */
public class SystemTime {
    public static  void main(String[] args)
    {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        System.out.println("Wow today is: " + dateFormat.format(date));
    }
}
