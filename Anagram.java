import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/18/2015.
 */
public class Anagram {
    public static void main(String[] args)
    {
        try
        {
            URL myUrl = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
            InputStreamReader isr = new InputStreamReader(myUrl.openStream());
            BufferedReader br = new BufferedReader(isr);

            String line = null;
            Scanner scan = new Scanner(System.in);
            String word = scan.next();
            int count = 0;
            String sortedWord = sortCharacter(word);
            String sortedLine = null;
            while((line = br.readLine()) != null)
            {


                    sortedLine = sortCharacter(line);

                    if(sortedWord.equalsIgnoreCase(sortedLine))
                    {
                        System.out.println(line);
                    }


            }


        }
        catch (Exception e)
        {

        }
    }
    public static String sortCharacter(String value)
    {

        char[] charArray = value.toCharArray();
        Arrays.sort(charArray);
        String valueToReturn = String.valueOf(charArray);
        return valueToReturn;
    }
}
