import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/18/2015.
 */
public class EvenAndOddNumbers {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        String message = "";
        if(num % 2 == 0)
        {
            message = "You've entered and even number";
        }
        else
        {
            message = "You've entered and odd number";
        }
        System.out.print(message);
    }
}
