import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by Mbhurhi on 10/9/2015.
 */
public class WarmUpExercise {

    public static void main(String[] args)
    {
        String[][] probabilities = { {"apples","0.25"},
                                     {"pears" , "0.5"},
                                    {"grapes" ,"0.2"},
                                    { "oranges","0.05"}};

        String[] fruits = new String[probabilities.length];
        double[] weights = new double[probabilities.length];

        System.out.println("Fruits and their weights before");
        for(int x = 0;x < probabilities.length;x++)
        {
            fruits[x] = probabilities[x][0];
            weights[x] += Double.parseDouble(probabilities[x][1]);
            System.out.println(fruits[x] + " => " + weights[x]);
        }


        double sum = 0;

        System.out.println("Fruits and their weights after");
        for(int x = 0;x < probabilities.length;x++)
        {
            fruits[x] = probabilities[x][0];
            sum += Double.parseDouble(probabilities[x][1]);
            weights[x] = sum;
            System.out.println(fruits[x] + " => " + weights[x]);
        }

        double start = 0;
        double end = 1;
        double random = new Random().nextDouble();
        double value = start + (random * (end - start));
        DecimalFormat df = new DecimalFormat("#.##");
        String fr = df.format(value);
        value = Double.valueOf(fr);
        String message = "";

        int i = 0;
        while(i <= weights.length)
        {
            if(value <= weights[i])
            {
                message = fruits[i] + " => " + weights[i];
                break;
            }
            i++;
        }

        System.out.println("Results");
        System.out.println("generated value is>>>>>>" + value);
        System.out.println(message);
    }

}
