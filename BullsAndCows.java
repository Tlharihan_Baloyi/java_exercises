import java.util.Random;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/30/2015.
 */
public class BullsAndCows {
    public static void main(String[] args)
    {

        int[] randomizedValues = randomNumbers();

        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter the 4 digit number to guess>>>>");


        String gues = scan.nextLine();


        int[] guessedValues = new int[gues.length()];
        for(int x=0;x < gues.length();x++)
        {
            guessedValues[x] = Integer.parseInt(String.valueOf(gues.charAt(x)));
        }


        int countBulls = 0;
        int countCows = 0;

        String guesedString = "";
        String randomizeString = "";

        String winMessage = "";

        int option = 0;

        for(int x = 0;x < guessedValues.length;x++)
        {
            guesedString += guessedValues[x];
            randomizeString += randomizedValues[x];
        }

        if(guesedString.equalsIgnoreCase(randomizeString))
           {
               winMessage = "Wow you won the game";
           }
            else
           {
               for(int  x = 0;x < guessedValues.length;x++)
               {
                   if(guessedValues[x] == randomizedValues[x])
                   {
                       countBulls ++;
                   }
                   else
                   {
                       for(int  y = 0;y < guessedValues.length;y++)
                       {
                           if(guessedValues[x] == randomizedValues[y])
                           {
                               countCows++;
                           }
                       }
                   }

               }
               winMessage = "you have "+ countBulls + " bulls and  " + countCows + " cows";
               option = 1;
           }

        if(countBulls == 0 && countCows == 0 && guesedString.equalsIgnoreCase(randomizeString))
        {
            winMessage = "you lost";
        }

        String gusValueMess = "";
        String ranValueMess = "";
        for(int gus: guessedValues)
        {
            gusValueMess += gus;
        }

        for(int ran: randomizedValues)
        {
            ranValueMess += ran;
        }


        System.out.println("Your guesed values:>>>>" + gusValueMess);
        System.out.println("System values:>>>>" + ranValueMess);
        System.out.println("result:>>>>" +  winMessage);

    }
    public static int generateNumber(int max)
    {

        Random ran = new Random();
        int value = ran.nextInt(max);
        if(value == 0)
        {
            value = 1;
        }
        return value;
    }
    public static int[] randomNumbers()
    {
        int[] generatedNumbers = new int[4];

        int number = 0;

        for(int x = 0;x < generatedNumbers.length;x++)
        {
            number =  generateNumber(10);
            if(x == 0)
            {
                generatedNumbers[x]= number;

            }
            else
            {
                for(int y=0;y <= x;y++)
                {
                    if(number != generatedNumbers[y])
                    {
                        generatedNumbers[x]= number;
                    }
                    else
                    {
                        if(y ==0)
                        {
                            while(number == generatedNumbers[y])
                            {
                                number =  generateNumber(10);
                            }
                        }

                        else if(y ==1)
                        {
                            while(number == generatedNumbers[y -1] || number == generatedNumbers[y])
                            {
                                number =  generateNumber(10);
                            }
                        }
                        else if(y ==2)
                        {
                            while(number == generatedNumbers[y -2] || number == generatedNumbers[y -1] || number == generatedNumbers[y])
                            {
                                number =  generateNumber(10);
                            }
                        }
                        else
                        {
                            while(number == generatedNumbers[y -3] || number == generatedNumbers[y -2] || number == generatedNumbers[y -1] || number == generatedNumbers[y])
                            {
                                number =  generateNumber(10);
                            }
                        }



                        generatedNumbers[x]= number;
                    }
                }
            }


        }


        return generatedNumbers;
    }

}
