import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/18/2015.
 */
public class AddingTwoNumbers {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        String twoNums = scan.nextLine();
        String[] nums = twoNums.split(" ");
        int num1 = Integer.parseInt(nums[0]);
        int num2 = Integer.parseInt(nums[1]);
        int sum = num1 +num2;

        System.out.println("Sum of two numbers is " + sum);
    }
}
