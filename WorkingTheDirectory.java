import java.io.File;
import java.io.FileFilter;

/**
 * Created by Mbhurhi on 9/30/2015.
 */
public class WorkingTheDirectory {
    public static void main(String[] args)
    {
        File file = new File("C:\\Users\\Mbhurhi\\Desktop\\java\\My_project\\src");
        File[] fileList = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".txt");
            }
        });

        if(fileList != null)
        {

            for(File f: fileList)
            {
                System.out.println( "File:" + f.getName());
            }
        }

    }
}
