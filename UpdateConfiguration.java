import java.io.*;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 10/6/2015.
 */
public class UpdateConfiguration {
    public static void main(String []args)
    {
        File myFile = new File("C:\\Users\\Mbhurhi\\IdeaProjects\\MyProjects\\src\\mabena.txt");
        try
        {
            Scanner scan = new Scanner(System.in);
            System.out.print("Please select the option.(1. To disable)or (2. To enable)or(3 to edit word in the line)or (4. To create a line if is not existing)>>>>");
             int option =scan.nextInt();

            if(option == 1)
            {
                System.out.print("Please select the word you want to disable>>>>");
                String choice = scan.next();


                String[] info = disable(myFile,choice);
                writeToFile(myFile,info);

            }
            else if(option == 3)
            {


                System.out.print("Please enter the word you want to replace>>>");
                String oldWord = scan.next();

                System.out.print("Please enter the replacement>>>");
                String newWord = scan.next();


                String[] info = edit(myFile, oldWord, newWord);
                writeToFile(myFile,info);
            }
            else if (option == 2)
            {
                System.out.print("Please select the word you want to enable>>>>");
                String choice = scan.next();
                String[] info = enable(myFile, choice);
                writeToFile(myFile,info);
            }
            else  if(option == 4)
            {
                System.out.print("Please enter the key word to check if the line does not exist>>>");
                String keyWord = scan.next();

                System.out.print("Please enter the value of the key word>>>");
                int value = scan.nextInt();

                String[] info = enableOrCreateLine(myFile,keyWord,value);

                writeToFile(myFile,info);
            }


        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public static int count(File file) throws Exception
    {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = null;


        int count =0;
        while((line = br.readLine()) != null)
        {
                count++;
        }
        return count;
    }

    public static String[] readInfo(File file) throws Exception
    {
        boolean status = true;
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        int count =0;
        String[] info = new String[count(file)];


        while((line = br.readLine()) != null)
        {
              info[count] = line;
                count++;

        }
        return info;
    }
    public static  String[] disable(File file,String disWord)throws Exception
    {
       String[] info = readInfo(file);


        for(int x= 0;x < info.length;x++)
        {
            if(!info[x].startsWith("#") && !info[x].isEmpty() && info[x].equalsIgnoreCase(disWord))
            {
                info[x] = "; " + info[x];
            }
        }


       return info;
    }
    public static  String[] enable(File file,String disWord)throws Exception
    {
        String[] info = readInfo(file);


        for(int x= 0;x < info.length;x++)
        {
            if(!info[x].startsWith("#") && !info[x].isEmpty())
            {
                String[] el = info[x].split(" ");

                String newLine = info[x];
                for(int y = 0;y < el.length;y++)
                {
                    if(el[y].equalsIgnoreCase(disWord))
                    {
                        newLine = disWord.toUpperCase();
                    }
                    info[x] = newLine;
                }
            }
        }


        return info;
    }
    public static  String[] edit(File file,String oldWord,String newWord)throws Exception
    {
        String[] info = readInfo(file);

        for(int x= 0;x < info.length;x++)
        {
            if(!info[x].startsWith("#") && !info[x].isEmpty())
            {
               String[] words = info[x].split(" ");
                String newLine = "";
                for(int y = 0;y < words.length;y++)
                {
                    if(words[y].equalsIgnoreCase(oldWord))
                    {
                        newLine += newWord + " ";
                    }
                    else
                    {
                        newLine += words[y] + " ";
                    }
                    info[x] = newLine;
                }

            }
        }


        return  info;
    }

    public static String[] enableOrCreateLine(File myFile,String keyWord,int value)throws Exception
    {
        String[] info = readInfo(myFile);

        int len = 0;

        for(int x= 0;x < info.length;x++)
        {
            String[] words = info[x].split(" ");


                for(int y = 0;y < words.length;y++)
                {
                    if(words[y].equalsIgnoreCase(keyWord))
                    {
                        len = info.length;
                        break;
                    }
                    else
                    {
                        len = info.length + 3;

                    }

                }
        }

        String[] newInfo = new String[len];

        if(newInfo.length == info.length)
        {
            for(int x = 0;x < info.length;x++)
            {
                newInfo[x] = info[x];
            }
        }
        else
        {
            for(int x = 0;x < info.length;x++)
            {
                newInfo[x] = info[x];
            }
            newInfo[info.length] = "" ;
            newInfo[info.length + 1] = "#  how many strawberries we have" ;
            newInfo[info.length + 2] = "" + keyWord + " " + value ;

        }


        return  newInfo;
    }
    public static void writeToFile(File myfile,String[] info)throws Exception
    {


        myfile.createNewFile();

        FileWriter fw = new FileWriter(myfile);
        BufferedWriter bw = new BufferedWriter(fw);

        for(String line:info)
        {
            bw.write(line);

            bw.newLine();
        }

        bw.close();

    }


}
