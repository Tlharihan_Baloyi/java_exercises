import javax.swing.*;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/29/2015.
 */
public class Sudoku {

    public static void main(String[] args) {

        int[][] grids = new int[9][9];
        grids[0][0] = 7;
        grids[0][3] = 1;
        grids[1][1] = 2;
        grids[1][7] = 1;
        grids[1][8] = 5;
        grids[2][5] = 6;
        grids[2][6] = 3;
        grids[2][7] = 9;
        grids[3][0] = 2;
        grids[3][4] = 1;
        grids[3][5] = 8;
        grids[4][1] = 4;
        grids[4][4] = 9;
        grids[4][7] = 7;
        grids[5][3] = 7;
        grids[5][4] = 5;
        grids[5][8] = 3;
        grids[6][1] = 7;
        grids[6][2] = 8;
        grids[6][3] = 5;
        grids[7][0] = 5;
        grids[7][1] = 6;
        grids[7][7] = 4;
        grids[8][5] = 1;
        grids[8][8] = 2;


        for (int rows = 0; rows < grids.length; rows++) {
            String message = "";
            for (int columns = 0; columns < grids.length; columns++) {
                message += grids[rows][columns] + " ";
            }
            System.out.println(message);
        }


        System.out.println("Play sudoku game by filling whats missing and at the position where there are value enter any thing but it will replaced by default ones");

        int[][] myGrids = playGame(grids);
       /*int[][] myGrids =  { {5,3,4,6,7,8,9,1,2},
                            {6,7,2,1,9,5,3,4,8},
                            {1,9,8,3,7,2,5,6,7},
                            {8,5,9,7,6,1,4,2,3},
                            {4,2,6,8,5,3,7,9,1},
                            {7,1,3,9,2,4,8,5,6},
                            {9,6,1,5,3,7,2,8,4},
                            {2,8,7,4,1,9,6,3,5},
                            {3,4,5,2,8,6,1,7,9}};
*/


        int rSum = 0;

        int cSum = 0;

        int[] rSumArray = new int[9];

        int[] cSumArray = new int[9];

        int[] boxSumArray = new int[9];


        for (int rows = 0; rows < myGrids.length; rows++) {
            String message = "";
            for (int columns = 0; columns < myGrids.length; columns++) {
                rSum += myGrids[rows][columns];
                cSum += myGrids[columns][rows];
                message += myGrids[rows][columns] + " ";

            }
            rSumArray[rows] = rSum;
            cSumArray[rows] = cSum;
            rSum = 0;
            cSum = 0;
            System.out.println(message);
        }


        for (int i = 0; i < myGrids.length; i++) {
            for (int j = 0; j < myGrids.length; j++) {
                if (i <= 2 && j <= 2) {
                    boxSumArray[0] += myGrids[i][j];
                }
                if (i <= 2 && (j >= 3 && j <= 5)) {
                    boxSumArray[1] += myGrids[i][j];
                }
                if (i <= 2 && (j >= 6 && j <= 8)) {
                    boxSumArray[2] += myGrids[i][j];
                }
                if ((i >= 3 && i <= 5) && (j <= 2)) {
                    boxSumArray[3] += myGrids[i][j];
                }
                if ((i >= 3 && i <= 5) && (j >= 3 && j <= 5)) {
                    boxSumArray[4] += myGrids[i][j];
                }
                if ((i >= 3 && i <= 5) && (j >= 6 && j <= 8)) {
                    boxSumArray[5] += myGrids[i][j];

                }
                if ((i >= 6) && (j <= 2)) {
                    boxSumArray[6] += myGrids[i][j];
                }
                if ((i >= 6) && (j >= 3 && j <= 5)) {
                    boxSumArray[7] += myGrids[i][j];
                }
                if ((i >= 6) && (j >= 6)) {
                    boxSumArray[8] += myGrids[i][j];
                }
            }

        }


        if(checkArrayStatus(rSumArray,cSum,boxSumArray))
        {
            System.out.println("The matrix is sudoku compliant");
        }
        else
        {
            System.out.println("The matrix is not sudoku compliant");
        }


    }
    public static int[][] playGame(int[][] grids)
    {

        int[][] innerGrids = new int[9][9];
        int[] myArray = new int[9];

        Scanner scan = new Scanner(System.in);
       int columns = 0;
        int count = 0;
        for(int rows = 0;rows < grids.length;rows++)
        {
            String message = "" ;

            System.out.print("Please enter values for row " + rows + " >>>>");
            String elements = scan.nextLine();
            String[] array  = elements.split(" ");
            for(int x = 0;x < array.length;x++)
            {
                myArray[x] = Integer.parseInt(array[x]);

            }
            for(int x = 0;x < myArray.length;x++)
            {
                innerGrids[rows][x] = myArray[x];

            }


        }
      /* innerGrids[0][0] = 7;
        innerGrids[0][3] = 1;
        innerGrids[1][1] = 2;
        innerGrids[1][7] = 1;
        innerGrids[1][8] = 5;
        innerGrids[2][5] = 6;
        innerGrids[2][6] = 3;
        innerGrids[2][7] = 9;
        innerGrids[3][0] = 2;
        innerGrids[3][4] = 1;
        innerGrids[3][5] = 8;
        innerGrids[4][1] = 4;
        innerGrids[4][4] = 9;
        innerGrids[4][7] = 7;
        innerGrids[5][3] = 7;
        innerGrids[5][4] = 5;
        innerGrids[5][8] = 3;
        innerGrids[6][1] = 7;
        innerGrids[6][2] = 8;
        innerGrids[6][3] = 5;
        innerGrids[7][0] = 5;
        innerGrids[7][1] = 6;
        innerGrids[7][7] = 4;
        innerGrids[8][5] = 1;
        innerGrids[8][8] = 2;*/
        return innerGrids;
    }



    public static boolean checkArrayStatus(int[] rowSum,int colSum,int[] boxSum)
    {

        int i =0;
        boolean status = true;

        while(i < 9)
        {
              if(rowSum[i] != 45  && boxSum[i] != 45)
              {
                   status = false;
                  break;
              }
            i++;
        }
        return status;
    }


}
