

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Scanner;
/**
 * Created by Mbhurhi on 10/2/2015.
 */
public class RPNCalculator {

    public static final String[] OPERATORS = { "+", "-", "*", "/" };


    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);


        String line = scan.nextLine();

        double evaluted = eval(line.split(" "));

        System.out.println("exp = " + evaluted);

    }
    public static Boolean isOperator(String token) {
        for(String op : OPERATORS) {
            if(op.equals(token)) {
                return true;
            }
        }
        return false;
    }


    public static Double operation(String op, Double e1, Double e2) {
        if(op.equals("+")) {
            return e1 + e2;
        }
        else if(op.equals("-")) {
            return e2 - e1;
        }
        else if(op.equals("*")) {
            return e1 * e2;
        }
        else if(op.equals("/")) {
            return e2 / e1;
        } else {
            throw new IllegalArgumentException("Invalid operator.");
        }
    }


    public static Double eval(String[] tokens) {
        LinkedList<Double> stack = new LinkedList<Double>();
        for(String token : tokens) {
            if (isOperator(token)) {
                stack.push(operation(token, stack.pop(), stack.pop()));
            }
            else {
                stack.push(Double.parseDouble(token));
            }
        }
        return stack.pop();
    }



}
