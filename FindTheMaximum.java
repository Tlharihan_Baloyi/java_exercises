/**
 * Created by Mbhurhi on 9/22/2015.
 */
public class FindTheMaximum {
    public static void main(String[] args)
    {
        int[] values ={1,3,6,4,56,22,64,23,564,35,67876,5554,343,5654,65,345,3543,2,65,654,456,45,35,234,87654,8,9,89,98};
        int highest = values[0];
        for(int index = 1;index < values.length;index++)
        {
            if(highest< values[index])
            {
                highest = values[index];
            }
        }
        System.out.println(highest);
    }
}
