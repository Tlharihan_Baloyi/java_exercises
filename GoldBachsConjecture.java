import java.util.Random;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/23/2015.
 */
public class GoldBachsConjecture {
    public static void main(String[] args)
    {
        System.out.print("Please enter an even number that you want to see its combination and it must be greater than 2 >>>>>>");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();


         if(number % 2 == 0 && number !=2) {

                 for (int x = 1; x < number / 2; x++) {
                     if (validatePrimeNumber(x) && validatePrimeNumber(number - x)) {
                         System.out.println(x + " + " + (number - x) + " = " + number);
                         x = number / 2;
                     }

                 }

         }

        else
         {
             System.out.print("Please enter the even number and please dont enter 2 as your input");
         }


    }

    public static boolean validatePrimeNumber(int number)
    {
        boolean test = true;
        for(int i=2;2*i<number;i++) {
            if(number%i==0)
                test =  false;
        }

        return test;
    }
}
