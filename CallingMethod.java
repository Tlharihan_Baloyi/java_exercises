import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/17/2015.
 */
public class CallingMethod extends  Mabena {


    public CallingMethod(String name, String surname, String favouriteProgramming) {
        super(name, surname, favouriteProgramming);
    }

    public static void main(String[] args)
    {
        methodWithoutArgs();



        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter your name:>>>>");
        String name = scan.nextLine();
        System.out.println("Please enter your surname:>>>>");
        String surname = scan.nextLine();
        System.out.println("Please enter the programming you like most:>>>>");
        String programing = scan.nextLine();


        String callingMethodWithVariable =  methodWithVariableNo(name, surname, programing);
        System.out.println(callingMethodWithVariable.toString());

        System.out.println();
        methodWithFixedNoOfArgs(name, surname, programing);
        System.out.println();
        methodWithOptionalArgs();
        System.out.println();
        methodWithOptionalArgs(name, surname);

        System.out.println();
        String returnedValue = obtainingAreturnedValue(name,surname,programing);
        System.out.println(returnedValue);

        String info = display(name, surname, programing);
        System.out.println();
        System.out.println(info);


        String callingProtectedMethod = display2(name, surname, programing);
        System.out.println();
        System.out.println(callingProtectedMethod);

        String callingPublicMethod = display3(name, surname, programing);
        System.out.println();
        System.out.println(callingPublicMethod);



    }
    public static void methodWithoutArgs()
    {
        String message = "It is very nice to call a method that doen't require arguments\n\n";
        System.out.println(message);
    }
    public static void methodWithFixedNoOfArgs(String name,String surname,String favouriteProgramming)
    {
        System.out.println("Wow!!!... " + name + " " + surname + " ,your favourite programming language is " + favouriteProgramming);
    }

    public static void methodWithOptionalArgs()
    {
        System.out.println("You method doesnt require arguments");
    }

    public static void methodWithOptionalArgs(String name,String surname)
    {
        System.out.println("You are overload arguments it this method" + name + " " + surname);
    }
    public static String obtainingAreturnedValue(String name,String surname,String programming)
    {
        String info = name + " " + surname + " love " + " programming so much of which is good thing";

        return info;
    }
    private static String display(String name,String surname,String favouriteProgramming)
    {
        String info = "Wow!!!... " + name + " " + surname + " ,your favourite programming language is " + favouriteProgramming + " and im calling private method";

        return  info;
    }
    public static String methodWithVariableNo(String... names)
    {
        String info = "";
        for(String name: names)
        {
            info += name + " ";
        }
        return info;
    }


}
class Mabena
{
    private String name;
    private String surname;
    private String programming;

    public Mabena(String name,String surname,String favouriteProgramming)
    {
        this.name = name;
        this.surname = surname;
        this.programming = favouriteProgramming;
    }
    public static String display3(String name,String surname,String favouriteProgramming)
    {
        String info = "Wow!!!... " + name + " " + surname + " ,your favourite programming language is " + favouriteProgramming + " and im calling public  method";

        return  info;
    }
    protected static String display2(String name,String surname,String favouriteProgramming)
    {
        String info = "Wow!!!... " + name + " " + surname + " ,your favourite programming language is " + favouriteProgramming + " and im calling protected method";

        return  info;
    }


}
