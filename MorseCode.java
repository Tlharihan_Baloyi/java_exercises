import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/23/2015.
 */
public class MorseCode {
    public static void main(String[] args)
    {

        Scanner scan = new Scanner(System.in);

        String[] alpha = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8",
                "9", "0"," "};
        String[] dottie = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
                "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
                "-.--", "--..", ".----", "..---", "...--", "....-", ".....",
                "-....", "--...", "---..", "----.", "-----"," "};




        System.out.print("Please select the option....1. to convert from string or number to morse....2. To convert form morse to String or number >>>");

         String option = scan.nextLine();


        if(option.equalsIgnoreCase("1"))
        {

            System.out.print("Please enter word or number you want to convert>>>>");
            String info = scan.nextLine();
            String convertedInfo = convertedString(alpha,dottie,info);
            System.out.println(convertedInfo);



        }
        else if(option.equalsIgnoreCase("2"))
        {
            System.out.println("Please enter morse code you want to convert");
            String info = scan.nextLine();
            String convertedInfo = convertedMorseCode(alpha, dottie, info);
            System.out.println(convertedInfo);

        }
        else
        {
            System.out.println("Please select the proper option...1 or 2 ");
        }


    }
    public static String convertedString(String[] alpha,String[] dottie,String word)
    {
        String convertedInfo = "";
        for(int x = 0;x< word.length();++x)
        {
            for(int y=0;y < alpha.length;y++)
            {
                if(String.valueOf(word.charAt(x)).equalsIgnoreCase(alpha[y]))
                {
                    convertedInfo = convertedInfo + dottie[y] + " ";
                }

            }

        }

        return convertedInfo;
    }
    public static String convertedMorseCode(String[] alpha,String[] dottie,String word)
    {
        String infor = word.replaceAll("  ", "#");
        String convertedInfo = "";

        String[] morseCode =  infor.split("#");
        for(String mCode: morseCode)
        {
            String[] msCode = mCode.split(" ");
            for(String mosCode: msCode)
            {
                for (int y = 0; y < dottie.length; y++) {

                    if (mosCode.equals(dottie[y])) {
                        convertedInfo = convertedInfo + alpha[y];
                    }


                }

            }
            convertedInfo += " ";


        }



        return convertedInfo;
    }



}
