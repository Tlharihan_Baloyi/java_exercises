import java.io.File;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 9/18/2015.
 */
public class DeleteFileAndDirectory {

    public static  void main(String[] args)
    {
        try
        {
            File file = new File("C:\\Users\\Mbhurhi\\Desktop\\java\\java_exercises\\src\\input.txt");
            File directory = new File("C:\\Users\\Mbhurhi\\Desktop\\java\\java_exercises\\src\\docs");
            file.createNewFile();
            directory.mkdir();


           System.out.println("Please enter (1 to delete file) or (2 to delete docs directory) or (any number to exit)>>>");
            Scanner scan = new Scanner(System.in);
            int option = scan.nextInt();

            if(option == 1)
            {
                file.deleteOnExit();
                if(file.delete()) {
                    System.out.println("File has deleted");
                }
            }
            else if(option == 2)
            {
                directory.deleteOnExit();
                if(directory.delete())
                {
                    System.out.println(directory.getName() + " directory is deleted");
                }
                else
                {
                    System.out.println(directory.getName() + " is not deleted");
                }
            }
            else
            {
                System.out.println("File is not deleted...you can press either option 1 to delete "+file.getName()+"file or option 2 to delete "+ directory.getName() + " directory");
            }


        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
