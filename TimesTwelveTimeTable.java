/**
 * Created by Mbhurhi on 9/21/2015.
 */
public class TimesTwelveTimeTable {

    public static void main(String[] args)
    {
        int max = 12;
        int product = 0;
        for(int x =1;x <= max;x++)
        {
            System.out.println();

            for(int y = 1;y <= max;y++)
            {
                product = x * y;
                System.out.println(x + " x " + y + " = " + product);
            }
        }
    }
}
