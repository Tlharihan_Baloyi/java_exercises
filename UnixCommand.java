import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by Mbhurhi on 10/2/2015.
 */
public class UnixCommand {
    public static void main(String[] args)
    {
        try {
            countWordsLinesCharacters();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void countWordsLinesCharacters()throws  Exception
    {
        try
        {
            File myfile = new File("C:\\Users\\Mbhurhi\\IdeaProjects\\MyProjects\\src\\mabena.txt");
            BufferedReader br = new BufferedReader(new FileReader(myfile));

            String line = null;

            String[] words;
            int countLines = 0;
            int countWords = 0;
            int countCharacters = 0;

            while((line = br.readLine()) != null)
            {
                words = line.split(" ");
                countWords += words.length;


                for(String word:words)
                {
                    if(word.isEmpty())
                    {
                        countWords -= 1;
                    }

                    countCharacters += word.length();
                }
                countLines++;
            }

            System.out.println("The file has " + countLines + " lines, " + countWords + " words and " + countCharacters + " characters");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
