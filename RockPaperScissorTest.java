import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Mbhurhi on 10/9/2015.
 */
public class RockPaperScissorTest {
    public static void main(String[] args)
    {
        ArrayList<String> rec = new ArrayList<>();
        ArrayList<String> cRec = new ArrayList<>();
        ArrayList<String> resRec = new ArrayList<>();

        Scanner scan = new Scanner(System.in);
        String option = "Y";
        int count = 0;
        while(option.equalsIgnoreCase("Y"))
        {
            if(rec.isEmpty())
            {
                count = 1;
                String playerChoice = playerChoice();
                String computerChoice =  computerChoice(rec);
                String result = results(playerChoice, computerChoice);
                if(!result.equalsIgnoreCase("You have entered wrong weapon"))
                {
                    rec.add(playerChoice);
                    cRec.add(computerChoice);
                    System.out.println("Results>>>" + result);

                }

                System.out.println("Player choice>>>" + playerChoice);
                System.out.println("Computer choice>>>" +computerChoice);
                System.out.println(result);
            }
            else
            {


                String playerChoice = playerChoice();
                String computerChoice =  computerChoice(rec);
                String result = results(playerChoice, computerChoice);
                if(!result.equalsIgnoreCase("You have entered wrong weapon"))
                {
                    rec.add(playerChoice);
                    rec.add(playerChoice);
                    cRec.add(computerChoice);
                    resRec.add(result);

                }

                System.out.println("Player choice>>>" + playerChoice);
                System.out.println("Computer choice>>>" +computerChoice);
                System.out.println("Results>>>" +result);

            }

            count++;
            System.out.print("Do you want to play again Y/N?");
            option = scan.nextLine();
        }

        for(String i: rec)
        {
            System.out.print(i);
        }


    }
    public static String playerChoice()
    {
        System.out.print("Please select the weapon of you choice(Rock, paper or scissor)>>>");
        Scanner scan = new Scanner(System.in);
        String choice = scan.nextLine();

        String[] choices = {"Paper","Rock","Scissor"};

        return  choice;

    }
    public static  String computerChoice(ArrayList<String> record)
    {
        int ran = new Random().nextInt(3);
        String choice = null;
        if(record.isEmpty())
        {
            if(ran == 0)
            {
                choice = "Rock";
            }
            else if(ran == 1)
            {
                choice = "paper";
            }
            else
            {
                choice = "Scissor";
            }
        }
        else if(record.size() == 1) {



            if(ran == 0)
            {
                choice = "Rock";
            }
            else if(ran == 1)
            {
                choice = "paper";
            }
            else
            {
                choice = "Scissor";
            }
            for (int col = 0; col < record.size(); col++) {
                if(record.get(col).equalsIgnoreCase("Rock"))
                {

                    while(ran == 1 || ran == 2)
                    {
                        if(ran == 1)
                        {
                            choice = "Paper";
                        }
                        else
                        {
                            choice = "scissor";
                        }
                        ran = new Random().nextInt(3);

                    }
                }
                else if(record.get(col).equalsIgnoreCase("paper"))
                {

                    while(ran == 0 || ran == 2)
                    {
                        if(ran == 0)
                        {
                            choice = "Rock";
                        }
                        else
                        {
                            choice = "Scissor";
                        }
                        ran = new Random().nextInt(3);

                    }
                }
                else
                {

                    while(ran == 0 || ran == 1)
                    {
                        if(ran == 0)
                        {
                            choice = "Rock";
                        }
                        else
                        {
                            choice = "paper";
                        }
                        ran = new Random().nextInt(3);

                    }
                }

            }
        }
        else
        {

            if(ran == 0)
            {
                choice = "Rock";
            }
            else if(ran == 1)
            {
                choice = "paper";
            }
            else
            {
                choice = "Scissor";
            }
            int rocks = 0;
            int papers = 0;
            int scissors = 0;
            for(int col = 0;col < record.size();col++)
            {
                if(record.get(col).equalsIgnoreCase("Rock"))
                {
                    rocks++;
                }
                else if(record.get(col).equalsIgnoreCase("Paper"))
                {
                    papers++;
                }
                else
                {
                    scissors++;
                }

            }
            if(rocks > papers && rocks > scissors && papers >scissors )
            {
                while(ran == 0 || ran == 1)
                {
                    if(ran == 0)
                    {
                        choice = "Rock";
                    }
                    else
                    {
                        choice = "paper";
                    }
                    ran = new Random().nextInt(3);

                }
            }


            else if(rocks > papers && rocks > scissors &&  scissors>papers )
            {
                while(ran == 0 || ran == 2)
                {
                    if(ran == 0)
                    {
                        choice = "Rock";
                    }
                    else
                    {
                        choice = "Scissor";
                    }
                    ran = new Random().nextInt(3);

                }
            }
            else if(papers > rocks && papers > scissors && rocks > scissors)
            {
                while(ran == 1 || ran == 0)
                {
                    if(ran == 0)
                    {
                        choice = "Rock";
                    }
                    else
                    {
                        choice = "paper";
                    }
                    ran = new Random().nextInt(3);
                }

            }

            else if(scissors > rocks && scissors > papers && papers > rocks)
            {
                while(ran == 1 || ran == 2 )
                {
                    if(ran == 1)
                    {
                        choice = "paper";
                    }
                    else
                    {
                        choice = "scissor";
                    }
                    ran = new Random().nextInt(3);
                }
            }
            else if(scissors > rocks && scissors > papers && rocks > papers)
            {
                while(ran == 0 || ran == 2 )
                {
                    if(ran == 0)
                    {
                        choice = "Rock";
                    }
                    else
                    {
                        choice = "scissor";
                    }
                    ran = new Random().nextInt(3);
                }
            }
            else if(scissors > rocks && scissors > papers &&  papers> rocks)
            {
                while(ran == 1 || ran == 2 )
                {
                    if(ran == 1)
                    {
                        choice = "paper";
                    }
                    else
                    {
                        choice = "scissor";
                    }
                    ran = new Random().nextInt(3);
                }
            }


        }

        return  choice;
    }
    public static String results(String playerChoice,String computersChoice) {
        String result = "";
        if (playerChoice.equalsIgnoreCase("rock") || playerChoice.equalsIgnoreCase("paper") || playerChoice.equalsIgnoreCase("scissor")) {
            if (computersChoice.equalsIgnoreCase("paper")) {
                if (playerChoice.equalsIgnoreCase("paper")) {
                    result = "Your choices are same...no 1 won the game";
                } else if (playerChoice.equalsIgnoreCase("scissor")) {
                    result = "Player won";
                } else {
                    result = "computer won";
                }
            } else if (computersChoice.equalsIgnoreCase("Rock")) {
                if (playerChoice.equalsIgnoreCase("Rock")) {
                    result = "Your choices are same...no 1 won the game";
                } else if (playerChoice.equalsIgnoreCase("Paper")) {
                    result = "Player won";
                } else {
                    result = "computer won";
                }
            } else {
                if (playerChoice.equalsIgnoreCase("Scissor")) {
                    result = "Your choices are same...no 1 won the game";
                } else if (playerChoice.equalsIgnoreCase("Rock")) {
                    result = "Player won";
                } else {
                    result = "computer won";
                }
            }

        }
        else
        {
            result = "You have entered wrong weapon";
        }
            return result;

        }



}
