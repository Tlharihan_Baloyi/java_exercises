import java.util.Scanner;

/**
 * Created by Mbhurhi on 10/2/2015.
 */
public class PanCakeFlipping {
    public static void main(String[] rags)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Please enter unique positive integers you want to sort in ascending order and seperate them by a space>>>>");
        String integers = scan.nextLine();

        String[] numbers = integers.split(" ");
        int[] arrayNums = new int[numbers.length];
        for(int y =0;y < numbers.length;y++)
        {
            arrayNums[y] = Integer.parseInt(numbers[y]);
        }

        int[] sortedInt = sortIntegers(arrayNums);

        for(int number: sortedInt)
        {
            System.out.println(number + " ");
        }

    }
    public static int[] sortIntegers(int[] unsortedIntegers)
    {

        int[] sortedIntegers = unsortedIntegers;

        for(int x =0;x < sortedIntegers.length - 1;x++)
        {
            for(int y =x;y < sortedIntegers.length;y++)
            {
                if(sortedIntegers[x] > sortedIntegers[y])
                {
                    int temp = unsortedIntegers[x];
                    sortedIntegers[x] = sortedIntegers[y];
                    sortedIntegers[y] = temp;
                }

            }
        }

        return sortedIntegers;
    }
}
